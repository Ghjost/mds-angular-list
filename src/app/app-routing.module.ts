import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BiereListComponent } from './pages/biere-list/biere-list.component';
import { GameListComponent } from './pages/game-list/game-list.component';

const routes: Routes = [
  // { path: '', component: AppComponent },
  { path: 'biere', component: BiereListComponent },
  { path: 'game', component: GameListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
