import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-biere-list',
  templateUrl: './biere-list.component.html',
  styleUrls: ['./biere-list.component.scss']
})
export class BiereListComponent implements OnInit {

  public data: any = [];
  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.data = this.getBiere();
  }

  getBiere(){
    return new Promise(
      (resolve,reject)=> {
        this.http.get('https://api.openbrewerydb.org/breweries/')
        .subscribe((data) => {
          this.data = data;
          resolve(data);
        },
        (err)=>{
          reject(err);
        });
      }
      )
  }


}

