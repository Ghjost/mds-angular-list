import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  public data: any = [];
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.data = this.getGames();
  }

  getGames(){
    return new Promise(
      (resolve,reject)=> {
        this.http.get('https://random-data-api.com/api/cannabis/random_cannabis?size=100')
        .subscribe((data) => {
          this.data = data;
          resolve(data);
          console.log(data)
        },
        (err)=>{
          reject(err);
        });
      }
      )
  }

}
